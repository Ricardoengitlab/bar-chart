import React from 'react';
import * as d3 from 'd3';
import './BarChart.css';

class BarChart extends React.Component{
    
    componentDidMount(){

        var req = new XMLHttpRequest();
        req.open("GET", 'https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/GDP-data.json', false);
        req.send();

        let data = {};
        data = JSON.parse(req.responseText);

        this.drawBarChart(data.data)
    }

    drawBarChart(data){  
        const canvasHeight = 710
        const canvasWidth = 1200
        const barwidth = canvasWidth/275
        const axis = 50

        var tooltip = d3.select(".visHolder").append("div")
                .attr("id", "tooltip")
                .style("opacity", 0);
        
        
        var overlay = d3.select('.visHolder').append('div')
        .attr('class', 'overlay')
        .style('opacity', 0);

        const svgCanvas = d3.select("div")
        .append("svg")
        .attr("width", canvasWidth)
        .attr("height", canvasHeight)
        .attr("id", "tooltip")
        .style("border", "1px solid black")
        .style("padding", "10px 10px")
        .style("margin", "20px  20%")
         
        var yearsDate = data.map(function(item) {
            return new Date(item[0]);
        });

        var xMax = new Date(d3.max(yearsDate));
        
        xMax.setMonth(xMax.getMonth() + 3);

        var xScale = d3.scaleTime()
            .domain([d3.min(yearsDate), xMax])
            .range([axis, canvasWidth]);

        var yScale = d3.scaleTime()
            .domain([0,18065])
            .range([0, canvasHeight-axis])


        var yscale = d3.scaleLinear()
            .domain([0, 18065])
            .range([canvasHeight-axis, 0]);

        var x_axis = d3.axisBottom()
            .scale(xScale);

        var y_axis = d3.axisLeft()
            .scale(yscale);

        svgCanvas.append("g")
            .attr("transform", "translate("+ axis + ", 0)")
            .attr("id", "y-axis")
            .call(y_axis);

        svgCanvas.append("g")
            .attr("transform", "translate(0,"+(canvasHeight-axis) + " )")
            .attr("id", "x-axis")
            .call(x_axis)
        
        svgCanvas.append("text")
        .attr("x", (canvasWidth/2))             
        .attr("y", 30)
        .attr("text-anchor", "middle")  
        .style("font-size", "20px") 
        .style("text-decoration", "underline")  
        .text("United States GDP");
        
        svgCanvas.selectAll("rect")
        .data(data).enter()
            .append("rect")
            .attr('data-date', function(d, i) {
                return d[0]
              })
            .attr('data-gdp', function(d, i) {
                return d[1]
              })
            .attr("width", barwidth)
            .attr("height", (datapoint) => yScale(datapoint[1]))
            .attr("fill", "turquoise")
            .attr("class", "bar")
            .attr("x", (datapoint, iteration) => xScale(yearsDate[iteration]))
            .attr("y", (datapoint) => canvasHeight - yScale(datapoint[1]) - axis )
            .on("mouseover", function(datapoint, iteration) {
                
              tooltip.transition()
                .duration(200)
                .style('opacity', .9);
              tooltip.html(datapoint[0] + '<br>' + datapoint[1] + ' Billion')
                .attr('data-date', datapoint[0])
            })
            .on("mouseout", function (d,i) {
                tooltip.transition()
                .duration(200)
                .style('opacity', 0);
              overlay.transition()
                .duration(200)
                .style('opacity', 0);
            });



    }

    render(){
        return(
            <div>
                <h1 id="title">United States GDP</h1>
                <div id="canvas">
                </div>
                <div className="visHolder">

                </div>
            </div>
        )
    }
}

export default BarChart;