import React from 'react';
import './App.css';
import BarChart from './BarChart';

class App extends React.Component{
  constructor(){
    super();
    this.state = {
    }
  }

  render(){
    return(
        <div className="App">
          <BarChart />
        </div>
    )
  }
}

export default App;
